package cn.jdw.silky.oauth2;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.client.InMemoryOAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.security.oauth2.client.registration.InMemoryClientRegistrationRepository;
import org.springframework.security.oauth2.client.web.AuthenticatedPrincipalOAuth2AuthorizedClientRepository;
import org.springframework.security.oauth2.client.web.OAuth2AuthorizedClientRepository;
import org.springframework.security.oauth2.core.AuthorizationGrantType;
import org.springframework.security.oauth2.core.ClientAuthenticationMethod;
import org.springframework.security.oauth2.core.oidc.IdTokenClaimNames;

/**
 * @author ListJiang
 * @since 2021/7/11 11:56
 * description 资源服务配置类，这里只做oauth客户端的校验配置，具体系统服务的登录校验交给security配置处理
 */
@Configuration
public class OAuth2Config {

    /**
     * 自定义 oauth 客户端仓库
     */
//    @Component
    class JdbcClientRegistrationRepository implements ClientRegistrationRepository {
        @Override
        public ClientRegistration findByRegistrationId(String registrationId) {
            return null;
        }
    }

    @Bean
    public OAuth2AuthorizedClientService authorizedClientService(
            ClientRegistrationRepository clientRegistrationRepository) {
        return new InMemoryOAuth2AuthorizedClientService(clientRegistrationRepository);
    }

    @Bean
    public OAuth2AuthorizedClientRepository authorizedClientRepository(
            OAuth2AuthorizedClientService authorizedClientService) {
        return new AuthenticatedPrincipalOAuth2AuthorizedClientRepository(authorizedClientService);
    }

    @Bean
    public ClientRegistrationRepository clientRegistrationRepository() {
        return new InMemoryClientRegistrationRepository(this.googleClientRegistration());
    }

    private ClientRegistration googleClientRegistration() {
        return ClientRegistration.withRegistrationId("registrationId")
                .clientId("clientId4")
                .clientSecret("secret")
                // oauth2.0 认证方法
                .clientAuthenticationMethod(ClientAuthenticationMethod.CLIENT_SECRET_POST)
                .authorizationGrantType(AuthorizationGrantType.AUTHORIZATION_CODE)
                // 授权服务授权 uri，该uri的host必须与 redirectUri 的host不一样，不然会导致session里面的
                // OAuth2ParameterNames.STATE值被清除，该地址登录用户浏览器需要访问
                .authorizationUri("http://192.168.0.207:80/oauth/authorize")
                .scope("openid", "profile", "email", "address", "phone")
                // 己方接收code Uri（也就是在授权服务器注册的 web_server_redirect_uri）
                .redirectUri("http://127.0.0.1:8081/login/oauth2/code/registrationId")
                // 己方通过code获取token，服务器需要访问
                .tokenUri("http://192.168.0.207:80/oauth/token")
                .userInfoUri("https://www.googleapis.com/oauth2/v3/userinfo")
                .userNameAttributeName(IdTokenClaimNames.SUB)
//                .jwkSetUri("https://www.googleapis.com/oauth2/v3/certs")
                .clientName("clientName")
                .build();
    }
}
