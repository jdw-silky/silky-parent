package cn.jdw.silky.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author ListJiang
 * description mybatis-plus配置
 * datetime 2021/7/6 20:33
 */
@Configuration
@MapperScan(value = {"cn.jdw.silky.*.mapper"})
public class MybatisPlusConfig {
}
