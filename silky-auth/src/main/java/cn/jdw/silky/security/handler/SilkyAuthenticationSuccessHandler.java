package cn.jdw.silky.security.handler;

import cn.jdw.silky.security.SilkyUser;
import lombok.extern.log4j.Log4j2;
import org.springframework.lang.NonNull;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;
import org.springframework.util.StringUtils;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author ListJiang
 * description 自定义认证成功处理器
 * datetime 2021/7/6 21:15
 */
@Log4j2
public class SilkyAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

    private String forwardUrl = "/";

    public SilkyAuthenticationSuccessHandler() {
    }

    public SilkyAuthenticationSuccessHandler(@NonNull String forwardUrl) {
        this.forwardUrl = forwardUrl;
    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        // 获取认证前用户访问的url地址，可选择是否跳回原地址
        RequestCache cache = new HttpSessionRequestCache();
        SavedRequest savedRequest = cache.getRequest(request, response);
        // 原始存的地址
        String url = savedRequest != null && StringUtils.hasLength(savedRequest.getRedirectUrl()) ?
                savedRequest.getRedirectUrl() : forwardUrl;
        log.info("重定向地址为：{}", url);
        // 通过认证对象获取用户信息
        SilkyUser user = (SilkyUser) authentication.getPrincipal();
        log.info("账号：{}", user.getUsername());
        log.info("权限：{}", user.getAuthorities());

        response.sendRedirect(url);
    }
}
