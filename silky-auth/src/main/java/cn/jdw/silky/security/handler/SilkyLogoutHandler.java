package cn.jdw.silky.security.handler;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.log4j.Log4j2;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

/**
 * @author ListJiang
 * description 自定义退出处理器
 * datetime 2021/7/6 21:30
 */
@Log4j2
@Component
public class SilkyLogoutHandler implements LogoutSuccessHandler {

    private static final String LOGOUT_TARGET_URI = "logout_target_uri";
    private static final String DEFAULT_LOGOUT_TARGET_URI = "/login";

    @Override
    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException {
        // 自定义退出操作
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        log.info("用户：{}退出登录成功！", userDetails.getUsername());
        // 重定向到登录页
        String url = request.getParameter(LOGOUT_TARGET_URI);
        if (StringUtils.hasText(url)) {
            url = new String(Base64.getDecoder().decode(url), StandardCharsets.UTF_8);
        }
        response.sendRedirect(StringUtils.hasText(url) ? url : DEFAULT_LOGOUT_TARGET_URI);

    }
}
