package cn.jdw.silky.security.handler;

import lombok.extern.log4j.Log4j2;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author ListJiang
 * description 自定义认证失败处理器
 * datetime 2021/7/6 20:53
 */
@Log4j2
public class SilkyAuthenticationFailureHandler implements AuthenticationFailureHandler {

    private final String forwardUrl;

    public SilkyAuthenticationFailureHandler(String forwardUrl) {
        this.forwardUrl = forwardUrl;
    }

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {
        request.setAttribute(WebAttributes.AUTHENTICATION_EXCEPTION, exception);
        log.debug("自定义退出重定向地址为：{}", forwardUrl);
        response.sendRedirect(forwardUrl);
    }
}
