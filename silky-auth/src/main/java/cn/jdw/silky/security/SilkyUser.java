package cn.jdw.silky.security;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;

/**
 * @author ListJiang
 * @since 2021/7/7 10:23
 * description Silky 用户实体
 */
public class SilkyUser implements UserDetails {

    private String username;
    private String password;
    private List<GrantedAuthority> authorityList;

    public SilkyUser(String username, String password, List<GrantedAuthority> authorityList) {
        this.username = username;
        this.password = password;
        this.authorityList = authorityList;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return this.authorityList;
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public String getUsername() {
        return this.username;
    }

    @Override
    public boolean isAccountNonExpired() {
        // 当前账号是否未过期
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        // 当前账号是否未锁定
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        // 用户凭证是否未过期
        return true;
    }

    @Override
    public boolean isEnabled() {
        // 用户是否启用
        return true;
    }
}
