package cn.jdw.silky.security;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author ListJiang
 * @since
 * datetime 2021/7/2 10:11
 */
@Configuration
@ConfigurationProperties(prefix = "silky")
public class SilkyAuthProperties {
    /**
     * jwt配置
     */
    private final Jwt jwt = new Jwt();

    public Jwt getJwt() {
        return jwt;
    }

    public static class Jwt {
        private String signingKey;

        public String getSigningKey() {
            return signingKey;
        }

        public void setSigningKey(String signingKey) {
            this.signingKey = signingKey;
        }
    }
}
