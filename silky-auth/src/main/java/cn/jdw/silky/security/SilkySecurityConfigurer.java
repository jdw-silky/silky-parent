package cn.jdw.silky.security;

import cn.jdw.silky.security.handler.SilkyAccessDeniedHandler;
import cn.jdw.silky.security.handler.SilkyAuthenticationSuccessHandler;
import cn.jdw.silky.security.handler.SilkyLogoutHandler;
import cn.jdw.silky.sys.service.impl.SilkyUserDetailsServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.security.oauth2.client.web.OAuth2AuthorizedClientRepository;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;

/**
 * @author ListJiang
 * description 认证配置
 * datetime 2021/7/6 20:31
 */
@EnableWebSecurity
@RequiredArgsConstructor
public class SilkySecurityConfigurer {
    private final SilkyAccessDeniedHandler customAccessDeniedHandler;
    private final SilkyUserDetailsServiceImpl customUserDetailService;
    private final SilkyLogoutHandler silkyLogoutHandler;
    private final PersistentTokenRepository persistentTokenRepository;
    private final ClientRegistrationRepository clientRegistrationRepository;
    private final OAuth2AuthorizedClientService oAuth2AuthorizedClientService;
    private final OAuth2AuthorizedClientRepository oAuth2AuthorizedClientRepository;

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity httpSecurity) throws Exception {
        // 基本登录设置
        return httpSecurity
                .formLogin(login ->
                                login

                                        // 登录请求url，默认请求方式为post
                                        // .loginProcessingUrl("/login")
                                        // 自定义登录请求参数名
                                        // .usernameParameter("username")
                                        // .passwordParameter("password")
                                        // 登录页面地址
                                        // .loginPage("/login")
                                        // 1).defaultSuccessUrl("/index") 该方法无论设置什么值都会导致成功跳转 “/" 地址

                                        // 2)登录成功跳转url，该地址只能是POST请求（默认设置）
                                        .successForwardUrl("/")
                                        // 3)设置自定义登录成功处理器，跳转外部系统页面以满足前后端分离项目的成功路径跳转需求
                                        .successHandler(new SilkyAuthenticationSuccessHandler())
                                        // 1）失败转发请求url配置，该地址必须是Get
                                        // 默认设置，该设置优先级比failureForwardUrl高，但是该方式url必须配置无需登录，
                                        // 不然失败后二次跳转会被认证拦截不然失败后二次跳转会被认证拦截
                                        .failureUrl("/login")
                        // 失败请求转发地址，该地址必须是POST请求
                        // 2）默认设置，该方式设置的url地址，在失败后可直接转发内容，不会走认证
                        // .failureForwardUrl("/toError")

                        // 3)设置自定义登录失败处理器，跳转外部系统页面以满足前后端分离项目的失败路径跳转需求
                        //.failureHandler(new SilkyAuthenticationFailureHandler("http://wwww.hao123.com"))
                )
                // 开启 remember-me 功能
                .rememberMe(remember ->
                        remember
                                // 登录逻辑
                                .userDetailsService(customUserDetailService)
                                // 持久层对象
                                .tokenRepository(persistentTokenRepository)
                )
                // oauth2Login配置
                .authorizeHttpRequests(authorize ->
                        authorize
                                .requestMatchers("/oauth/**", "/oauth2/**").permitAll()
                )
                .oauth2Login(oauth2 ->
                        oauth2
                                .clientRegistrationRepository(clientRegistrationRepository)
                                .authorizedClientRepository(oAuth2AuthorizedClientRepository)
                                .authorizedClientService(oAuth2AuthorizedClientService)
                )
                .authorizeHttpRequests(authorizeHttpRequests ->
                        authorizeHttpRequests
                                // 只要登录了就可以看用户信息
                                .requestMatchers("/user/**", "/sys/user/**", "/logout/**").authenticated()
                                .requestMatchers("/login", "/login/**").permitAll()
                                .requestMatchers("/actuator/**").permitAll()
                )
                // 权限校验设置
                .authorizeRequests(authorize ->
                        authorize
                                // 使用自定义权限校验剩余所有请求
                                .anyRequest().access("@permissionServiceImpl.hasPermission(request,authentication)")
                )
                // 关闭csrf防护
                .csrf(AbstractHttpConfigurer::disable)
                // 自定义异常处理器(权限不足的访问，认证的已经在authorizeRequests配置了)
                .exceptionHandling(handling -> handling.accessDeniedHandler(customAccessDeniedHandler))
                // 自定义退出逻辑
                .logout(logout ->
                        logout
                                .logoutUrl("/logout")
                                .logoutSuccessHandler(silkyLogoutHandler))
                .build();

    }

}
