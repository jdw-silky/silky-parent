package cn.jdw.silky.sys.service;

import cn.jdw.silky.sys.entity.Role;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色表 服务类
 * </p>
 *
 * @author ListJiang
 * @since 2021-07-07
 */
public interface IRoleService extends IService<Role> {

}
