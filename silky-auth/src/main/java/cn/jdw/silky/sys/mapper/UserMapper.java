package cn.jdw.silky.sys.mapper;

import cn.jdw.silky.sys.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author ListJiang
 * @since 2021-07-07
 */
public interface UserMapper extends BaseMapper<User> {

}
