package cn.jdw.silky.sys.service.impl;

import cn.jdw.silky.sys.entity.Account;
import cn.jdw.silky.sys.mapper.AccountMapper;
import cn.jdw.silky.sys.service.IAccountService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 账号表 服务实现类
 * </p>
 *
 * @author ListJiang
 * @since 2021-07-07
 */
@Service
public class AccountServiceImpl extends ServiceImpl<AccountMapper, Account> implements IAccountService {

}
