package cn.jdw.silky.sys.service;

import cn.jdw.silky.sys.entity.Account;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 账号表 服务类
 * </p>
 *
 * @author ListJiang
 * @since 2021-07-07
 */
public interface IAccountService extends IService<Account> {

}
