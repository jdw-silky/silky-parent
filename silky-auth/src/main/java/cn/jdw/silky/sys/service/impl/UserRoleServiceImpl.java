package cn.jdw.silky.sys.service.impl;

import cn.jdw.silky.sys.entity.UserRole;
import cn.jdw.silky.sys.mapper.UserRoleMapper;
import cn.jdw.silky.sys.service.IUserRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户角色关系表	 服务实现类
 * </p>
 *
 * @author ListJiang
 * @since 2021-07-07
 */
@Service
public class UserRoleServiceImpl extends ServiceImpl<UserRoleMapper, UserRole> implements IUserRoleService {

}
