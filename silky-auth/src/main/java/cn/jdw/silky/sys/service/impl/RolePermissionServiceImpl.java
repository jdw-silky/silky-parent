package cn.jdw.silky.sys.service.impl;

import cn.jdw.silky.sys.entity.RolePermission;
import cn.jdw.silky.sys.mapper.RolePermissionMapper;
import cn.jdw.silky.sys.service.IRolePermissionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色权限关系表	 服务实现类
 * </p>
 *
 * @author ListJiang
 * @since 2021-07-07
 */
@Service
public class RolePermissionServiceImpl extends ServiceImpl<RolePermissionMapper, RolePermission> implements IRolePermissionService {

}
