package cn.jdw.silky.sys.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 账号表
 * </p>
 *
 * @author ListJiang
 * @since 2021-07-07
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_account")
public class Account implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id，账号id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 账号
     */
    private String account;

    /**
     * 密码
     */
    private String password;

    /**
     * 创建人账号id
     */
    private Long creator;

    /**
     * 创建时间
     */
    private LocalDateTime creteTime;

    /**
     * 修改人账号id
     */
    private Long updator;

    /**
     * 修改时间
     */
    private LocalDateTime updateTime;

    /**
     * 逻辑删除标识：1，删除。0，未删除
     */
    private Boolean delFlag;


}
