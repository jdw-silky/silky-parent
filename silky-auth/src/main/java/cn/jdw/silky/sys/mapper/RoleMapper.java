package cn.jdw.silky.sys.mapper;

import cn.jdw.silky.sys.entity.Role;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * 角色表 Mapper 接口
 * </p>
 *
 * @author ListJiang
 * @since 2021-07-07
 */
public interface RoleMapper extends BaseMapper<Role> {

    /**
     * 通过用户id查询角色集合
     *
     * @param id 用户id
     * @return 角色集合
     */
    List<Role> getRoleByUserId(Long id);
}
