package cn.jdw.silky.sys.mapper;

import cn.jdw.silky.sys.entity.Account;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 账号表 Mapper 接口
 * </p>
 *
 * @author ListJiang
 * @since 2021-07-07
 */
public interface AccountMapper extends BaseMapper<Account> {

}
