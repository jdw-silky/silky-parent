package cn.jdw.silky.sys.service.impl;

import cn.jdw.silky.sys.entity.User;
import cn.jdw.silky.sys.mapper.UserMapper;
import cn.jdw.silky.sys.service.IUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author ListJiang
 * @since 2021-07-07
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

}
