package cn.jdw.silky.sys.service.impl;

import cn.jdw.silky.security.SilkyUser;
import cn.jdw.silky.sys.entity.Account;
import cn.jdw.silky.sys.entity.Permission;
import cn.jdw.silky.sys.entity.Role;
import cn.jdw.silky.sys.entity.User;
import cn.jdw.silky.sys.service.IAccountService;
import cn.jdw.silky.sys.service.IUserService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

/**
 * @author ListJiang
 * @since 2021/7/7 10:12
 * description
 */
@Service
public class SilkyUserDetailsServiceImpl implements UserDetailsService {

    public static final String ROLE_PREFIX = "ROLE_";
    @Autowired
    private IAccountService iAccountService;
    @Autowired
    private RoleServiceImpl roleService;
    @Autowired
    private PermissionServiceImpl permissionService;
    @Autowired
    private IUserService iUserService;


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
// 角色权限说明：在Spring Security 中都是 authority 资源（字符串）。带有 ROLE_ 前缀的就是角色，不带的就是资源。
        // ROLE_ 严格区分大小写

        // step1: get account;
        Account account = iAccountService.getOne(new LambdaQueryWrapper<Account>().eq(Account::getAccount, username));
        if (account == null) {
            throw new UsernameNotFoundException("用户" + username + "不存在");
        }
        User jdwUser = iUserService.getOne(new LambdaQueryWrapper<User>().eq(User::getAccountId, account.getId()));

        // step2: get role、permission
        List<String> authorityList = new LinkedList<>();
        List<Long> roleIds = new LinkedList<>();
        // 获取角色
        List<Role> roleByUserId = roleService.getBaseMapper().getRoleByUserId(jdwUser.getId());
        if (!roleByUserId.isEmpty()) {
            roleByUserId.forEach(role -> {
                roleIds.add(role.getId());
                authorityList.add(ROLE_PREFIX + role.getCode());
            });
            // 获取权限
            List<Permission> permissions = permissionService.getBaseMapper().getPermissionByRoleIds(roleIds);
            if (!permissions.isEmpty()) {
                permissions.forEach(jdwPermission -> authorityList.add(jdwPermission.getCode()));
            }
        }

        // step3：return 自定义user
        return new SilkyUser(username, account.getPassword(), AuthorityUtils.createAuthorityList(authorityList.toArray(new String[0])));
    }
}
