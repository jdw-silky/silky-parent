package cn.jdw.silky.sys.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.Map;

/**
 * @author ListJiang
 * @since 2021/7/7 17:18
 * description 首页控制器
 */
@RestController
public class IndexController {
    @RequestMapping("/")
    public Map index() {
        return Collections.singletonMap("key", "value");
    }
}
