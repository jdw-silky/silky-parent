package cn.jdw.silky.sys.service.impl;

import cn.jdw.silky.security.SilkyUser;
import cn.jdw.silky.sys.entity.Permission;
import cn.jdw.silky.sys.mapper.PermissionMapper;
import cn.jdw.silky.sys.service.IPermissionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;

import jakarta.servlet.http.HttpServletRequest;
import java.util.Collection;

/**
 * <p>
 * 权限表 服务实现类
 * </p>
 *
 * @author ListJiang
 * @since 2021-07-07
 */
@Service
public class PermissionServiceImpl extends ServiceImpl<PermissionMapper, Permission> implements IPermissionService {

    @Override
    public boolean hasPermission(HttpServletRequest request, Authentication authentication) {
        Object principal = authentication.getPrincipal();
        if (principal instanceof SilkyUser silkyUser) {
            Collection<? extends GrantedAuthority> authorities = silkyUser.getAuthorities();
            return authorities.contains(new SimpleGrantedAuthority(request.getRequestURI()));
        }
        return false;
    }
}
