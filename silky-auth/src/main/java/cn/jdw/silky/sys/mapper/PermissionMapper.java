package cn.jdw.silky.sys.mapper;

import cn.jdw.silky.sys.entity.Permission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * 权限表 Mapper 接口
 * </p>
 *
 * @author ListJiang
 * @since 2021-07-07
 */
public interface PermissionMapper extends BaseMapper<Permission> {

    /**
     * 通过角色id集合查询权限集合
     *
     * @param roleIds 角色id集合
     * @return 权限集合
     */
    List<Permission> getPermissionByRoleIds(List<Long> roleIds);
}
