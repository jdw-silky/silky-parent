package cn.jdw.silky.sys.service;

import cn.jdw.silky.sys.entity.RolePermission;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色权限关系表	 服务类
 * </p>
 *
 * @author ListJiang
 * @since 2021-07-07
 */
public interface IRolePermissionService extends IService<RolePermission> {

}
