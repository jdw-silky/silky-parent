package cn.jdw.silky.sys.service.impl;

import cn.jdw.silky.sys.entity.Role;
import cn.jdw.silky.sys.mapper.RoleMapper;
import cn.jdw.silky.sys.service.IRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色表 服务实现类
 * </p>
 *
 * @author ListJiang
 * @since 2021-07-07
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements IRoleService {

}
