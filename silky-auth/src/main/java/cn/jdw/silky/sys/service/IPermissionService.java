package cn.jdw.silky.sys.service;

import cn.jdw.silky.sys.entity.Permission;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.security.core.Authentication;

import jakarta.servlet.http.HttpServletRequest;

/**
 * <p>
 * 权限表 服务类
 * </p>
 *
 * @author ListJiang
 * @since 2021-07-07
 */
public interface IPermissionService extends IService<Permission> {
    /**
     * 自定义鉴权
     *
     * @param request        请求
     * @param authentication 认证用户
     * @return 是否拥有权限
     */
    boolean hasPermission(HttpServletRequest request, Authentication authentication);
}
