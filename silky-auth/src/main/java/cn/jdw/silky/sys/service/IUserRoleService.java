package cn.jdw.silky.sys.service;

import cn.jdw.silky.sys.entity.UserRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户角色关系表	 服务类
 * </p>
 *
 * @author ListJiang
 * @since 2021-07-07
 */
public interface IUserRoleService extends IService<UserRole> {

}
