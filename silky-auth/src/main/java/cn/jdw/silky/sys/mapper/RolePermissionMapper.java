package cn.jdw.silky.sys.mapper;

import cn.jdw.silky.sys.entity.RolePermission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色权限关系表	 Mapper 接口
 * </p>
 *
 * @author ListJiang
 * @since 2021-07-07
 */
public interface RolePermissionMapper extends BaseMapper<RolePermission> {

}
