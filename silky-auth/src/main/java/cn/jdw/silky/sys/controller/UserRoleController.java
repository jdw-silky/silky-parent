package cn.jdw.silky.sys.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 用户角色关系表	 前端控制器
 * </p>
 *
 * @author ListJiang
 * @since 2021-07-07
 */
@RestController
@RequestMapping("/sys/user-role")
public class UserRoleController {

}
