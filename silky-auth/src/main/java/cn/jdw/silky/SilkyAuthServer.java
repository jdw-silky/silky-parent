package cn.jdw.silky;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author ListJiang
 * @since 1.0.1
 * description 认证服务
 * datetime 2021/7/1 22:34
 */
@SpringBootApplication
public class SilkyAuthServer {
    public static void main(String[] args) {
        SpringApplication.run(SilkyAuthServer.class, args);
    }
}


