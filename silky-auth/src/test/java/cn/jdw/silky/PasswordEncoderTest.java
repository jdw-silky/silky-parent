package cn.jdw.silky;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * @author ListJiang
 * datetime 2021/7/6 20:43
 */
public class PasswordEncoderTest {
    private static final Logger log = LoggerFactory.getLogger(PasswordEncoderTest.class);

    @Test
    public void BCryptPasswordEncoderTest() {
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        String plaintext = "admin";
        String ciphertext = bCryptPasswordEncoder.encode(plaintext);
        log.info("明文：{}" , plaintext);
        log.info("密文：{}" , ciphertext);
        log.info("匹配：{}" , bCryptPasswordEncoder.matches(plaintext, ciphertext));
        // $2a$10$U5/1RuWmFvOQs.D5NY/8.eIbP5EwnpoGqNC72d6HhCFFEs6KIQ2SW
        // $2a$10$jbHD9bKvoNhXNBba1OvTUOvfdlUB5hQ6h4leDn8ni8D5nv2yiySBy
        // $2a$10$IL6t0/ojvCJNk8ypp47oLOjvbyeLj3Z/CAZfpCuZuM1VncQi4CT2y
    }
}
