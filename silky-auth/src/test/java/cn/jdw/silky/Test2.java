package cn.jdw.silky;

import java.util.concurrent.locks.LockSupport;

public class Test2 {
    public static void main(String[] args) throws InterruptedException {
        SThread sThread = new SThread(Thread.currentThread());
//        Thread.sleep(3000);
        System.out.println("before park");
        // 获取许可
        sThread.start();
        LockSupport.park("ParkAndUnparkDemo");
        System.out.println("after park");
    }
}

class SThread extends Thread{

    private Object object;

    public SThread(Object object) {
        this.object = object;
    }

    public void run() {
        System.out.println("before unpark");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        // 获取blocker
        System.out.println("Blocker info " + LockSupport.getBlocker((Thread) object));
        // 释放许可
        LockSupport.unpark((Thread) object);
        // 休眠500ms，保证先执行park中的setBlocker(t, null);
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        // 再次获取blocker
        System.out.println("Blocker info " + LockSupport.getBlocker((Thread) object));

        System.out.println("after unpark");
    }

}
