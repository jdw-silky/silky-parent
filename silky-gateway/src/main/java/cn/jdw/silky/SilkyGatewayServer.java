package cn.jdw.silky;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cloud.gateway.event.RefreshRoutesResultEvent;
import org.springframework.cloud.gateway.route.CachingRouteLocator;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Bean;

/**
 * @author ListJiang
 */
@SpringBootApplication
@EnableDiscoveryClient
public class SilkyGatewayServer {
    public static void main(String[] args) {
        SpringApplication.run(SilkyGatewayServer.class, args);
    }

    @Bean
    RouteLocator gateway(RouteLocatorBuilder rlb) {
        return rlb.routes()
                .route(predicateSpec -> predicateSpec
                        .path("/silky-demo-web/**")
                        .uri("lb://silky-demo-web"))
                .build();
    }

    @Bean
    @ConditionalOnProperty(value = "spring.profiles.active",havingValue = "dev")
    ApplicationListener<RefreshRoutesResultEvent> applicationListener() {
        return event -> {
            var crl = (CachingRouteLocator) event.getSource();
            crl.getRoutes().subscribe(System.out::println);
        };
    }

}
