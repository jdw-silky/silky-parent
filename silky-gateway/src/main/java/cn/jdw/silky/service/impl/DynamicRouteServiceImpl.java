package cn.jdw.silky.service.impl;

import cn.jdw.silky.service.DynamicRouteService;
import lombok.AllArgsConstructor;
import org.springframework.cloud.gateway.event.RefreshRoutesEvent;
import org.springframework.cloud.gateway.route.RouteDefinition;
import org.springframework.cloud.gateway.route.RouteDefinitionRepository;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * @author ListJiang
 * date 2020/8/17 16:10
 */
@Service
@AllArgsConstructor
public class DynamicRouteServiceImpl implements DynamicRouteService, ApplicationEventPublisherAware {

    private RouteDefinitionRepository repository;
    private ApplicationEventPublisher publisher;


    /**
     * 发送路由刷新事件
     */
    private void publisherRouteEvent() {
        this.publisher.publishEvent(new RefreshRoutesEvent(this));
    }

    @Override
    public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
        this.publisher = applicationEventPublisher;
    }

    @Override
    public Mono<Void> save(Mono<RouteDefinition> definition) {
        return repository.save(definition).then();
    }

    @Override
    public Flux<Void> saveBatch(Flux<RouteDefinition> routeDefinitions) {
        return routeDefinitions.flatMap(t -> repository.save(Mono.justOrEmpty(t)));
    }

    @Override
    public Mono<Void> delete(Mono<String> routeId) {
        return repository.delete(routeId);
    }

    @Override
    public Flux<Void> deleteBatch(Flux<String> routeIds) {
        return routeIds.flatMap(t -> repository.delete(Mono.justOrEmpty(t)));
    }
}
