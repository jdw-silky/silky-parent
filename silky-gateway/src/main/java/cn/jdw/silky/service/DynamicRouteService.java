package cn.jdw.silky.service;

import org.springframework.cloud.gateway.route.RouteDefinition;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * @author ListJiang
 * interface 动态路由接口
 * description
 * datetime 2021/6/29 12:37
 */

public interface DynamicRouteService {
    /**
     * 增加路由
     *
     * @param definition 路由定义
     * @return 增加路由成功标识
     */
    Mono<Void> save(Mono<RouteDefinition> definition);

    /**
     * 批量增加路由
     *
     * @param routeDefinitions 路由定义集合
     * @return 批量增加路由成功标识
     */
    Flux<Void> saveBatch(Flux<RouteDefinition> routeDefinitions);

    /**
     * 删除路由路由
     *
     * @param routeId 路由id
     * @return 删除路由路由成功标识
     */
    Mono<Void> delete(Mono<String> routeId);

    /**
     * 批量删除路由路由
     *
     * @param routeIds 路由id集合
     * @return 批量删除路由路由成功标识
     */
    Flux<Void> deleteBatch(Flux<String> routeIds);
}
