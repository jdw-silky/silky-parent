package cn.jdw.silky.config;

import com.alibaba.fastjson2.JSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.route.RouteDefinition;
import org.springframework.cloud.gateway.route.RouteDefinitionRepository;
import org.springframework.cloud.gateway.support.NotFoundException;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.redis.core.ReactiveStringRedisTemplate;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Objects;

/**
 * @author ListJiang
 * description  自定义redis路由仓库
 * datetime 2021/6/29 10:26
 */
@Component
public class RedisRouteDefinitionRepository implements RouteDefinitionRepository {

    public static final String GATEWAY_ROUTES = "GATEWAY_ROUTES";
    @Autowired
    @Lazy
    private ReactiveStringRedisTemplate stringRedisTemplate;

    @Override
    public Flux<RouteDefinition> getRouteDefinitions() {
        return stringRedisTemplate.opsForHash().values(GATEWAY_ROUTES)
                .filter(Objects::nonNull).map(json -> {
                    System.out.println(json.toString());
                    return JSON.parseObject(json.toString(), RouteDefinition.class);
                });
    }

    @Override
    public Mono<Void> save(Mono<RouteDefinition> route) {
        return route.flatMap(r -> stringRedisTemplate.opsForHash().put(GATEWAY_ROUTES, r.getId(), r))
                .flatMap(state -> state ?
                        Mono.empty() : Mono.error(new IllegalArgumentException("redisRoute save Exception")));
    }

    @Override
    public Mono<Void> delete(Mono<String> routeId) {
        return routeId.flatMap(id -> stringRedisTemplate.opsForHash().hasKey(GATEWAY_ROUTES, id)
                .flatMap(hasKey -> {
                    if (hasKey) {
                        return Mono.empty();
                    } else {
                        return Mono.error(new NotFoundException("路由文件没有找到: " + routeId));
                    }
                })
        );
    }
}
