package cn.jdw.silky.config;

import org.springframework.boot.autoconfigure.data.redis.RedisProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.ReactiveRedisConnectionFactory;
import org.springframework.data.redis.connection.RedisNode;
import org.springframework.data.redis.connection.RedisSentinelConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.ReactiveStringRedisTemplate;

/**
 * @author ListJiang
 * datetime 2021/5/25 21:17
 */
@Configuration
public class RedisConfig {

    private final RedisProperties redisProperties;

    public RedisConfig(RedisProperties redisProperties) {
        this.redisProperties = redisProperties;
    }


    @Bean
    public LettuceConnectionFactory lettuceConnectionFactory() {
        RedisProperties.Sentinel sentinel = redisProperties.getSentinel();
        RedisSentinelConfiguration sentinelConfig = new RedisSentinelConfiguration()
                .master(sentinel.getMaster());
        for (String t : sentinel.getNodes()) {
            RedisNode redisNode = new RedisNode(t.split(":")[0], Integer.parseInt(t.split(":")[1]));
            sentinelConfig.sentinel(redisNode);
        }
        sentinelConfig.setMaster(sentinel.getMaster());
        sentinelConfig.setSentinelPassword(sentinel.getPassword());
        sentinelConfig.setPassword(redisProperties.getPassword());
        sentinelConfig.setUsername(redisProperties.getUsername());
        sentinelConfig.setDatabase(redisProperties.getDatabase());
        return new LettuceConnectionFactory(sentinelConfig);
    }

    @Bean
    public ReactiveStringRedisTemplate reactiveStringRedisTemplate(ReactiveRedisConnectionFactory rrcf) {
        return new ReactiveStringRedisTemplate(rrcf);
    }

}
