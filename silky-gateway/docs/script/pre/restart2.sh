#!/bin/bash
# 环境变量
env=pre
port=8762
app_file=silky-gateway-1.0.1.jar
log_file=silky-gateway.log
echo 将要启动的jar包:${app_file}
pid=`ps -ef |grep "${app_file}" | grep -v grep | awk '{print $2}'`
echo 将要杀死原来的进程$pid
kill -9 $pid
echo "$pid进程终止成功"
#kill -9 `ps -ef |grep "${app_file}" | grep -v grep | awk '{print $2}'`
sleep 2
echo "判断jar包${app_file}文件是否存在，如果存在启动${app_file}包"
if test -e $app_file
then
	echo "文件存在,开始以${env}环境启动此程序..."
	# 启动jar包，指向日志文件，2>&1 & 表示打开或指向同一个日志文件
	nohup java -Xms1024m -Xmx1024m -Xmn512m -Xss256k -XX:SurvivorRatio=8 -jar "${app_file}" --spring.profiles.active=${env} --server.port=${port} > ${log_file} 2>&1 &
	tail -f ${log_file}
	#echo "$app_file 启动成功..."
else
	echo "$app_file 文件不存在,请检查。"
fi
