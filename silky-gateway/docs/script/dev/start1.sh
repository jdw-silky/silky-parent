#!/bin/sh
# 环境变量
env=dev
port=8765
app_name=silky-gateway-1.0.1.jar
log_file=silky-gateway.log
echo ${env}环境启动文件平台jar包:${app_name}
nohup java -XX:MaxDirectMemorySize=9999M -Xms1024m -Xmx1024m -Xmn512m -Xss256k -XX:SurvivorRatio=8 -jar ${app_name} --spring.profiles.active=${env} --server.port=${port} > ${log_file} 2>&1 &
tail -f ./${log_file}
