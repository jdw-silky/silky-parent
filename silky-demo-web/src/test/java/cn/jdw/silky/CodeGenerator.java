package cn.jdw.silky;

import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @Author ListJiang
 * @Since class mybatis-plus代码生成器
 * datetime 2021/10/5 14:30
 */
public class CodeGenerator {

    // 处理 all 情况
    protected static List<String> getTables(String tables) {
        return "all".equals(tables) ? Collections.emptyList() : Arrays.asList(tables.split(","));
    }

    public static void main(String[] args) {
        final String url = "jdbc:oracle:thin:@//172.31.1.201:1521/keydev";
        final String user = "std_db";
        final String password = "123456";
        final String projectPath = "D:\\ideaProject\\gitee\\jdw-silky\\silky-sso-server\\authorization-server\\src\\main\\java";
        FastAutoGenerator.create(url, user, password)
                // 全局配置
                .globalConfig((scanner, builder) ->
                        builder.author(scanner.apply("请输入作者名称？"))
                                .enableSwagger()
                                .outputDir(projectPath))
                // 包配置
                .packageConfig((scanner, builder) -> builder.parent(scanner.apply("请输入包名？")))
                // 策略配置
                .strategyConfig((scanner, builder) -> builder.addInclude(getTables(scanner.apply("请输入表名，多个英文逗号分隔？所有输入" +
                                " all")))
                        .controllerBuilder().enableRestStyle().enableHyphenStyle()
                        .entityBuilder().enableLombok().columnNaming(NamingStrategy.no_change)
                        .build())
                .templateEngine(new FreemarkerTemplateEngine())
                .execute();
    }
}