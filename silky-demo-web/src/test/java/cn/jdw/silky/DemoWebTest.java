package cn.jdw.silky;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.netflix.eureka.EurekaClientAutoConfiguration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

/**
 * @Author ListJiang
 * @Since 1.0.1
 * datetime 2021/7/1 13:30
 */
@ActiveProfiles("native")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class DemoWebTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    @ConditionalOnClass(value = EurekaClientAutoConfiguration.class)
    void contextLoads() throws Exception {

        mockMvc.perform(MockMvcRequestBuilders.get("/producer/sleep/{seconds}", 1))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(print());
    }

}
