package cn.jdw.silky.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum Sex {
    UNKNOWN("0", "未知"),
    MAN("1", "男"),
    WOMAN("2", "女");
    @EnumValue
    @JsonValue
    private final String code;
    private final String name;
}
