package cn.jdw.silky;

import cn.jdw.silky.contains.AppConstants;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;

/**
 * @author ListJiang
 * 声明
 * 备注
 * 创建时间 2021/6/26 11:12
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
public class DemoWebServer {
    public static void main(String[] args) {
        SpringApplication.run(DemoWebServer.class, args);
    }
}


@RestController
@RequiredArgsConstructor
class DemoController {
    private final DemoClient demoClient;

    /**
     * 模拟服务消费
     */
    @RequestMapping("/consumer/sleep/{seconds}")
    public String consumerSleep(@PathVariable int seconds) {
        return demoClient.sleep(seconds);
    }


    /**
     * 模拟服务提供
     */
    @RequestMapping("/producer/sleep/{seconds}")
    public String producerSleep(@PathVariable int seconds) throws InterruptedException {
        TimeUnit.SECONDS.sleep(seconds);
        return AppConstants.getPort() + "服务休眠了：" + seconds + "秒";
    }
}


@FeignClient(name = "silky-demo-web")
interface DemoClient {

    @LoadBalanced
    @GetMapping(value = "/producer/sleep/{seconds}")
    String sleep(@PathVariable(value = "seconds") int seconds);
}

