package cn.jdw.silky.test.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 角色权限关系表	 前端控制器
 * </p>
 *
 * @author ListJiang
 * @since 2021-07-01
 */
@RestController
@RequestMapping("/test/sys-role-permission")
public class SysRolePermissionController {

}
