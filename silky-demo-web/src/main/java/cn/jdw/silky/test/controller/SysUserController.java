package cn.jdw.silky.test.controller;


import cn.jdw.silky.test.service.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * <p>
 * 用户表 前端控制器
 * </p>
 *
 * @author ListJiang
 * @since 2021-07-01
 */
@RestController
@RequestMapping("/test/sys-user")
@RefreshScope
public class SysUserController {

    @Autowired
    private ISysUserService iSysUserService;
    @Value("${config.name}")
    private String name;

    @GetMapping("/{id}")
    public Object get(@PathVariable String id) {
        return name + iSysUserService.getById(id).getNickName();
    }
}
