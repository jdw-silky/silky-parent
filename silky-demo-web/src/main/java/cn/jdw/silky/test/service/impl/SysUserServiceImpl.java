package cn.jdw.silky.test.service.impl;

import cn.jdw.silky.test.entity.SysUser;
import cn.jdw.silky.test.mapper.SysUserMapper;
import cn.jdw.silky.test.service.ISysUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author ListJiang
 * @since 2021-07-01
 */
@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements ISysUserService {

}
