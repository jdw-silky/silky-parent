package cn.jdw.silky.test.mapper;

import cn.jdw.silky.test.entity.SysUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author ListJiang
 * @since 2021-07-01
 */
public interface SysUserMapper extends BaseMapper<SysUser> {

}
