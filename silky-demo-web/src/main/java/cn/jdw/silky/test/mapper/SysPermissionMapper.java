package cn.jdw.silky.test.mapper;

import cn.jdw.silky.test.entity.SysPermission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 权限表 Mapper 接口
 * </p>
 *
 * @author ListJiang
 * @since 2021-07-01
 */
public interface SysPermissionMapper extends BaseMapper<SysPermission> {

}
