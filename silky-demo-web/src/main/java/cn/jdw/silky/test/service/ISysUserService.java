package cn.jdw.silky.test.service;

import cn.jdw.silky.test.entity.SysUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author ListJiang
 * @since 2021-07-01
 */
public interface ISysUserService extends IService<SysUser> {

}
