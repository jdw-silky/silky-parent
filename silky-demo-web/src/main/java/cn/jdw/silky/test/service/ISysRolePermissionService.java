package cn.jdw.silky.test.service;

import cn.jdw.silky.test.entity.SysRolePermission;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色权限关系表	 服务类
 * </p>
 *
 * @author ListJiang
 * @since 2021-07-01
 */
public interface ISysRolePermissionService extends IService<SysRolePermission> {

}
