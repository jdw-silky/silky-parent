package cn.jdw.silky.test.service;

import cn.jdw.silky.test.entity.SysRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色表 服务类
 * </p>
 *
 * @author ListJiang
 * @since 2021-07-01
 */
public interface ISysRoleService extends IService<SysRole> {

}
