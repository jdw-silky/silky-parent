package cn.jdw.silky.test.service.impl;

import cn.jdw.silky.test.entity.SysPermission;
import cn.jdw.silky.test.mapper.SysPermissionMapper;
import cn.jdw.silky.test.service.ISysPermissionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 权限表 服务实现类
 * </p>
 *
 * @author ListJiang
 * @since 2021-07-01
 */
@Service
public class SysPermissionServiceImpl extends ServiceImpl<SysPermissionMapper, SysPermission> implements ISysPermissionService {

}
