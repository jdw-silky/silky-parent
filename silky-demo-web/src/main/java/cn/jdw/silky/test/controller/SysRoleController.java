package cn.jdw.silky.test.controller;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 角色表 前端控制器
 * </p>
 *
 * @author ListJiang
 * @since 2021-07-01
 */
@RestController
@RequestMapping("/test/sys-role")
public class SysRoleController {

    @GetMapping("/{id}")
    public Object get(@PathVariable String id) {
        return "这是一个：" + id;
    }
}
