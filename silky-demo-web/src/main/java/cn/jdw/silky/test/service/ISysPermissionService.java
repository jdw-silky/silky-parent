package cn.jdw.silky.test.service;

import cn.jdw.silky.test.entity.SysPermission;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 权限表 服务类
 * </p>
 *
 * @author ListJiang
 * @since 2021-07-01
 */
public interface ISysPermissionService extends IService<SysPermission> {

}
