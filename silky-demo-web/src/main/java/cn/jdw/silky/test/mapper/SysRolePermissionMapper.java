package cn.jdw.silky.test.mapper;

import cn.jdw.silky.test.entity.SysRolePermission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色权限关系表	 Mapper 接口
 * </p>
 *
 * @author ListJiang
 * @since 2021-07-01
 */
public interface SysRolePermissionMapper extends BaseMapper<SysRolePermission> {

}
