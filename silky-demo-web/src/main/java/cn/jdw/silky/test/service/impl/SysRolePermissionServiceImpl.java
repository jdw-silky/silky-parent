package cn.jdw.silky.test.service.impl;

import cn.jdw.silky.test.entity.SysRolePermission;
import cn.jdw.silky.test.mapper.SysRolePermissionMapper;
import cn.jdw.silky.test.service.ISysRolePermissionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色权限关系表	 服务实现类
 * </p>
 *
 * @author ListJiang
 * @since 2021-07-01
 */
@Service
public class SysRolePermissionServiceImpl extends ServiceImpl<SysRolePermissionMapper, SysRolePermission> implements ISysRolePermissionService {

}
