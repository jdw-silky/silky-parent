package cn.jdw.silky.test.service;

import cn.jdw.silky.test.entity.SysUserRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户角色关系表	 服务类
 * </p>
 *
 * @author ListJiang
 * @since 2021-07-01
 */
public interface ISysUserRoleService extends IService<SysUserRole> {

}
