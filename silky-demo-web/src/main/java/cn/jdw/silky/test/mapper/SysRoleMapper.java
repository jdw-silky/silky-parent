package cn.jdw.silky.test.mapper;

import cn.jdw.silky.test.entity.SysRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色表 Mapper 接口
 * </p>
 *
 * @author ListJiang
 * @since 2021-07-01
 */
public interface SysRoleMapper extends BaseMapper<SysRole> {

}
