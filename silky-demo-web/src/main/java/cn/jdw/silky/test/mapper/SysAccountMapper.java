package cn.jdw.silky.test.mapper;

import cn.jdw.silky.test.entity.SysAccount;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 账号表 Mapper 接口
 * </p>
 *
 * @author ListJiang
 * @since 2021-07-01
 */
public interface SysAccountMapper extends BaseMapper<SysAccount> {

}
