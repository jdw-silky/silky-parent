package cn.jdw.silky.test.service.impl;

import cn.jdw.silky.test.entity.SysUserRole;
import cn.jdw.silky.test.mapper.SysUserRoleMapper;
import cn.jdw.silky.test.service.ISysUserRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户角色关系表	 服务实现类
 * </p>
 *
 * @author ListJiang
 * @since 2021-07-01
 */
@Service
public class SysUserRoleServiceImpl extends ServiceImpl<SysUserRoleMapper, SysUserRole> implements ISysUserRoleService {

}
