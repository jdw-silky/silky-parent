package cn.jdw.silky.test.service.impl;

import cn.jdw.silky.test.entity.SysAccount;
import cn.jdw.silky.test.mapper.SysAccountMapper;
import cn.jdw.silky.test.service.ISysAccountService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 账号表 服务实现类
 * </p>
 *
 * @author ListJiang
 * @since 2021-07-01
 */
@Service
public class SysAccountServiceImpl extends ServiceImpl<SysAccountMapper, SysAccount> implements ISysAccountService {

}
