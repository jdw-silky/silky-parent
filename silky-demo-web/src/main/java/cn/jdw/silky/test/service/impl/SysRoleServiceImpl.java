package cn.jdw.silky.test.service.impl;

import cn.jdw.silky.test.entity.SysRole;
import cn.jdw.silky.test.mapper.SysRoleMapper;
import cn.jdw.silky.test.service.ISysRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色表 服务实现类
 * </p>
 *
 * @author ListJiang
 * @since 2021-07-01
 */
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements ISysRoleService {

}
