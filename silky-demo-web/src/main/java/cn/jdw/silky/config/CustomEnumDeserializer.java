package cn.jdw.silky.config;

import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

@Slf4j
public class CustomEnumDeserializer<T> extends JsonDeserializer<T> {
    private final ConcurrentHashMap<String, T> enumMap = new ConcurrentHashMap<>();

    public CustomEnumDeserializer(Class<T> enumeClass) {
        Method jsonMethod = null;
        Field jsonField = null;
        for (Field field : enumeClass.getDeclaredFields()) {
            for (Annotation annotation : field.getDeclaredAnnotations()) {
                if (annotation instanceof JsonValue) {
                    jsonField = field;
                    break;
                }
            }
        }
        for (Method declaredMethod : enumeClass.getDeclaredMethods()) {
            for (Annotation annotation : declaredMethod.getDeclaredAnnotations()) {
                if (annotation instanceof JsonValue) {
                    jsonMethod = declaredMethod;
                    break;
                }
            }
        }
        if (jsonField != null) {
            boolean accessible = jsonField.isAccessible();
            jsonField.setAccessible(true);
            for (Field field : enumeClass.getFields()) {
                try {
                    Object enumVal = field.get(null);
                    if (enumeClass.isInstance(enumVal)) {
                        enumMap.put(Objects.toString(jsonField.get(enumVal), ""), (T) enumVal);
                    }
                } catch (IllegalAccessException e) {
                    log.warn("自定义 JSON 反序列化获取属性出错", e);
                }
            }
            jsonField.setAccessible(accessible);
        }
        if (jsonMethod != null) {
            boolean accessible = jsonMethod.isAccessible();
            jsonMethod.setAccessible(true);
            for (Field field : enumeClass.getFields()) {
                try {
                    Object enumVal = field.get(null);
                    if (enumeClass.isInstance(enumVal)) {
                        enumMap.put(Objects.toString(jsonMethod.invoke(enumVal), ""), (T) enumVal);
                    }
                } catch (IllegalAccessException | InvocationTargetException e) {
                    log.warn("自定义 JSON 反序列化调用方法出错", e);
                }
            }
            jsonMethod.setAccessible(accessible);
        }
    }

    @Override
    public T deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
        return enumMap.get(p.getText());
    }
}
