package cn.jdw.silky.config;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

/**
 * @author ListJiang
 * @since description
 * datetime 2021/7/1 20:51
 */
@Configuration
public class DataSourceConfig {

    @Bean
    @RefreshScope
    @ConfigurationProperties(prefix = "spring.datasource")
    public DataSource dataSource(DataSourceProperties dataSourceProperties) {
        HikariDataSource hd = new HikariDataSource();
        hd.setJdbcUrl(dataSourceProperties.getUrl());
        hd.setUsername(dataSourceProperties.getUsername());
        hd.setPassword(dataSourceProperties.getPassword());
        hd.setDriverClassName(dataSourceProperties.getDriverClassName());
        return hd;
    }

}
