package cn.jdw.silky.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author ListJiang
 * @since
 * description mybatis-plus配置
 * datetime 2021/7/1 14:54
 */
@Configuration
@MapperScan(value = "cn.jdw.silky.*.mapper")
public class MybatisPlusConfig {
}
