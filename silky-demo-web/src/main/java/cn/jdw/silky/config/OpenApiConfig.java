package cn.jdw.silky.config;

import io.swagger.v3.oas.models.ExternalDocumentation;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import org.springdoc.core.customizers.OpenApiCustomizer;
import org.springdoc.core.customizers.OperationCustomizer;
import org.springdoc.core.models.GroupedOpenApi;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.autoconfigure.endpoint.web.WebEndpointProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import static org.springdoc.core.utils.Constants.ALL_PATTERN;

@Configuration
public class OpenApiConfig {
    @Value("${springdoc.version}")
    String appVersion;

    @Bean
    @Profile("!prod")
    public GroupedOpenApi actuatorApi(OpenApiCustomizer actuatorOpenApiCustomiser,
                                      OperationCustomizer actuatorCustomizer,
                                      WebEndpointProperties endpointProperties
    ) {
        return GroupedOpenApi.builder()
                .group("Actuator")
                .pathsToMatch(endpointProperties.getBasePath() + ALL_PATTERN)
                .addOpenApiCustomizer(actuatorOpenApiCustomiser)
                .addOpenApiCustomizer(openApi -> openApi.info(new Info().title("Actuator API").version(appVersion)))
                .addOperationCustomizer(actuatorCustomizer)
                .pathsToExclude("/health/*")
                .build();
    }

    @Bean
    public GroupedOpenApi miApi() {
        return GroupedOpenApi.builder()
                .group("数据同步")
                .packagesToScan("com.creator.kyydatasync.controller")
                .build();
    }

    @Bean
    public OpenAPI springShopOpenAPI() {
        Contact contact = new Contact();
        contact.setName("dewen.jiang");
        contact.setEmail("dewen.jiang@chinacreator.com");
        contact.setUrl("http://www.chinacreator.com/");
        return new OpenAPI()
                .info(new Info().title("文书解析接口文档")
                        .contact(contact)
                        .description("文书解析系统")
                        .version(appVersion))
                .externalDocs(new ExternalDocumentation()
                        .description("公司介绍")
                        .url("http://www.chinacreator.com/cn/"));
    }


}
