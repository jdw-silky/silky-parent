package cn.jdw.silky;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.square.retrofit.EnableRetrofitClients;

/**
 * @author ListJiang
 * 启动类声明开启 EnableRetrofitClients
 */
@SpringBootApplication
@EnableRetrofitClients
@EnableDiscoveryClient
public class SquareWebServer {
    public static void main(String[] args) {
        SpringApplication.run(SquareWebServer.class, args);
    }
}

