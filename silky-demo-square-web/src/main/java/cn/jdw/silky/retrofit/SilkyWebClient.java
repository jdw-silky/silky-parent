package cn.jdw.silky.retrofit;

import org.springframework.cloud.square.retrofit.core.RetrofitClient;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

@RetrofitClient("silky-demo-web")
public interface SilkyWebClient {
    @GET("/producer/sleep/{seconds}")
    Call<String> sleep(@Path("seconds") int seconds);
}
