package cn.jdw.silky.controller;

import cn.jdw.silky.retrofit.SilkyWebClient;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
@RequiredArgsConstructor
public class ConsumeController {
    private final SilkyWebClient silkyWebClient;

    @GetMapping("/consumer/sleep/{seconds}")
    String sleep(@PathVariable("seconds") int seconds) throws IOException {
        return silkyWebClient.sleep(seconds).execute().body();
    }
}
