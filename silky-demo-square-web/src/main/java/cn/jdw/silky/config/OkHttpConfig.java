package cn.jdw.silky.config;

import okhttp3.OkHttpClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author ListJiang
 */
@Configuration
public class OkHttpConfig {

    @Bean
    @LoadBalanced
    public OkHttpClient.Builder builder() {
        return new OkHttpClient.Builder();
    }

}
