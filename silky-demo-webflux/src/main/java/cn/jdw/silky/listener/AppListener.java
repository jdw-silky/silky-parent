package cn.jdw.silky.listener;

import cn.jdw.silky.contains.AppConstants;
import org.springframework.boot.web.context.WebServerInitializedEvent;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;

@Configuration
public class AppListener {

    @EventListener
    public void portReady(WebServerInitializedEvent event) {
        AppConstants.setPort(event.getWebServer().getPort());
    }
}
