package cn.jdw.silky.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.autoconfigure.endpoint.web.WebEndpointProperties;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.core.userdetails.MapReactiveUserDetailsService;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.server.SecurityWebFilterChain;

import static org.springframework.security.config.Customizer.withDefaults;

/**
 * author ListJiang
 * class security配置
 * remark
 * date 2021/6/13 22:09
 */
@Configuration
@EnableWebFluxSecurity
public class SecurityConfig {
    @Autowired
    WebEndpointProperties webEndpointProperties;
    @Autowired
    private SecurityProperties securityProperties;


    @Bean
    public MapReactiveUserDetailsService userDetailsService() {

        UserDetails user = User.withUsername(securityProperties.getUser().getName())
                .password(securityProperties.getUser().getPassword())
                .roles(securityProperties.getUser().getRoles().toArray(new String[0]))
                .build();
        return new MapReactiveUserDetailsService(user);
    }

    @Bean
    public SecurityWebFilterChain springSecurityFilterChain(ServerHttpSecurity http) {
        http.csrf().disable();
        http
                .authorizeExchange(exchanges -> exchanges
                        .pathMatchers(webEndpointProperties.getBasePath() + "/**").authenticated()
                        .anyExchange().permitAll()
                )
                .httpBasic(withDefaults())
                .formLogin(withDefaults());
        return http.build();
    }
}
