package cn.jdw.silky.controller;

import cn.jdw.silky.contains.AppConstants;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
public class PortController {

    @GetMapping("/port/{name}")
    public Mono<String> name(@PathVariable String name) {
        return Mono.just("服务端口:" + AppConstants.getPort() + " 调用名字：" + name);
    }
}
