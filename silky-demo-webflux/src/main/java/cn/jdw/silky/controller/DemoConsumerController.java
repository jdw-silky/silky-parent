package cn.jdw.silky.controller;

import org.springframework.cloud.client.loadbalancer.reactive.ReactorLoadBalancerExchangeFilterFunction;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * @author ListJiang
 * remark 服务消费
 * date 2021/6/14 19:45
 */
@RestController
@RequestMapping("/consumer")
public class DemoConsumerController {

    private static final String SERVICE = "http://silky-demo-webflux";

    private final WebClient.Builder loadBalancedWebClientBuilder;
    private final ReactorLoadBalancerExchangeFilterFunction lbFunction;

    public DemoConsumerController(WebClient.Builder loadBalancedWebClientBuilder,
                                  ReactorLoadBalancerExchangeFilterFunction lbFunction
    ) {
        this.loadBalancedWebClientBuilder = loadBalancedWebClientBuilder;
        this.lbFunction = lbFunction;
    }

    @RequestMapping("/mono")
    public Mono<String> mono(@RequestParam(value = "name", defaultValue = "Mary") String name) {
        return loadBalancedWebClientBuilder.build().get().uri(SERVICE + "/producer/mono")
                .retrieve().bodyToMono(String.class)
                .map(greeting -> "%s, %s!".formatted(greeting, name));
    }

    @RequestMapping("/flux")
    public Flux<String> flux(@RequestParam(value = "name", defaultValue = "John") String name) {
        return WebClient.builder().filter(lbFunction).build().get().uri(SERVICE + "/producer/flux", name).retrieve().bodyToFlux(String.class);
    }

    @RequestMapping("/hi")
    public Mono<String> hi(@RequestParam(value = "name", defaultValue = "Mary") String name) {
        return loadBalancedWebClientBuilder.build().get().uri(SERVICE + "/producer/greeting")
                .retrieve().bodyToMono(String.class)
                .map(greeting -> String.format("%s, %s!", greeting, name));
    }

    @RequestMapping("/hello")
    public Mono<String> hello(@RequestParam(value = "name", defaultValue = "John") String name) {
        return WebClient.builder().filter(lbFunction).build().get().uri(SERVICE + "/producer/greeting").retrieve().bodyToMono(String.class)
                .map(greeting -> String.format("%s, %s!", greeting, name));
    }


    @GetMapping("/port/{name}")
    public Mono<String> port(@PathVariable(value = "name") String name) {
        return loadBalancedWebClientBuilder.build().get().uri(SERVICE + "/port/" + name)
                .retrieve().bodyToMono(String.class);
    }
}
