package cn.jdw.silky.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * @author ListJiang
 * remark 服务提供
 * date 2021/6/14 19:45
 * TODO：差个基于声明式注解的响应式服务远程调用，目前官方暂未提供 2021-06-26
 */
@RestController
@RequestMapping("/producer")
public class DemoProducerController {
    private static Logger log = LoggerFactory.getLogger(DemoProducerController.class);


    @RequestMapping(value = "/flux", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<String> flux() {
        return Flux.fromStream(IntStream.range(1, 10).mapToObj(t -> {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return Thread.currentThread().getName() + "" + t;
        }));
    }

    @RequestMapping("/mono")
    public Mono<String> mono() {
        return Mono.just("这是一个mono1");
    }

    @RequestMapping(value = "/test3", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<String> test3() {
        System.out.println(Thread.currentThread().getName());
        Stream<String> stringStream = IntStream.range(1, 40).mapToObj(t -> {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return Thread.currentThread().getName() + "" + t;
        });

        System.out.println(Thread.currentThread().getName());
        return Flux.fromStream(stringStream);
    }

    @RequestMapping("/greeting")
    public String greet() {
        log.info("Access /greeting");

        List<String> greetings = Arrays.asList("1Hi there", "1Greetings", "1Salutations");
        Random rand = new Random();

        int randomNum = rand.nextInt(greetings.size());
        return greetings.get(randomNum);
    }

    @RequestMapping("/")
    public String home() {
        log.info("Access /");
        return "1Hi!";
    }

    @RequestMapping("/sleep/{seconds}")
    public Mono<String> sleep(@PathVariable int seconds) throws InterruptedException {
        TimeUnit.SECONDS.sleep(seconds);
        return Mono.just("sleep:" + seconds + "s");
    }
}
