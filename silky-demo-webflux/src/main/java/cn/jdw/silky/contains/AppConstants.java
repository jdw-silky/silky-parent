package cn.jdw.silky.contains;

public final class AppConstants {
    private AppConstants() {
    }

    private static int port = 0;

    public static void setPort(int port) {
        AppConstants.port = port;
    }

    public static int getPort() {
        return port;
    }
}

