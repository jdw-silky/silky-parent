package cn.jdw.silky;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author ListJiang
 * 声明
 * 备注
 * 创建时间 2021/6/26 16:23
 */
@SpringBootApplication
public class DemoWebFluxServer {
    public static void main(String[] args) {
        SpringApplication.run(DemoWebFluxServer.class, args);
    }
}
