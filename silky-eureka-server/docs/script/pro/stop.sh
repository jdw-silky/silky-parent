#!/bin/sh
# 杀死eureka服务进程
app_file=silky-eureka-server-1.0.1.jar
log_file=silky-eureka-server.log
echo 杀死eureka服务进程jar包:${app_file}
cp ${app_file} ./backup/$(date "+%Y-%m-%d_%H:%M:%S")${app_file}
mv ${log_file} ./backup/$(date "+%Y-%m-%d_%H:%M:%S")${log_file}
ps -ef | grep ${app_file} | grep -v grep | awk '{print $2}' | xargs kill -9
