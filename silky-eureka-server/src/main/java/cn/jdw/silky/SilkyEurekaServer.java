package cn.jdw.silky;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * @author ListJiang
 */
@SpringBootApplication
@EnableEurekaServer
public class SilkyEurekaServer {
    public static void main(String[] args) {
        SpringApplication.run(SilkyEurekaServer.class, args);
    }
}
