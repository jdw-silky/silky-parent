package cn.jdw.silky;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

/**
 * @author ListJiang
 */
@SpringBootApplication
@EnableConfigServer
public class SilkyConfigServer {
    public static void main(String[] args) {
        SpringApplication.run(SilkyConfigServer.class,args);
    }
}
