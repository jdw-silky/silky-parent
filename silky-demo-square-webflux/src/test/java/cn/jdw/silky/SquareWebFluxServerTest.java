package cn.jdw.silky;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.util.stream.IntStream;

/**
 * @author ListJiang
 */
@ActiveProfiles("native")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SquareWebFluxServerTest {
    @Autowired
    private WebTestClient webClient;

    //@Test
    void contextLoads() {
        IntStream.range(1, 20).parallel().forEach(i -> {
            var result = webClient.get().uri("/producer/sleep/3")
                    .accept(MediaType.APPLICATION_JSON)
                    .exchange()
                    .expectStatus().isOk()
                    .expectBody(String.class).returnResult().getResponseBody();
            System.out.println(result);
        });
    }
}
