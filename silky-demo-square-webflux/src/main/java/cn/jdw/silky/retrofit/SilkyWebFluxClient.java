package cn.jdw.silky.retrofit;

import org.springframework.cloud.square.retrofit.core.RetrofitClient;
import reactor.core.publisher.Mono;
import retrofit2.http.GET;
import retrofit2.http.Path;

@RetrofitClient("silky-demo-webflux")
public interface SilkyWebFluxClient {
    @GET("/port/{port}")
    Mono<String> port(@Path("port") int name);
}
