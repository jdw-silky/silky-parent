package cn.jdw.silky;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.square.retrofit.webclient.EnableRetrofitClients;

/**
 * @author ListJiang
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableRetrofitClients
public class SquareWebFluxServer {
    public static void main(String[] args) {
        SpringApplication.run(SquareWebFluxServer.class, args);
    }
}

