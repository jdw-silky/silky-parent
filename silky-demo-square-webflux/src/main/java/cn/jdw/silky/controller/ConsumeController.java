package cn.jdw.silky.controller;

import cn.jdw.silky.retrofit.SilkyWebFluxClient;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@RequiredArgsConstructor
public class ConsumeController {
    private final SilkyWebFluxClient silkyWebFluxClient;

    @GetMapping("/producer/sleep/{port}")
    Mono<String> sleep(@PathVariable("port") int port) {
        return silkyWebFluxClient.port(port);
    }
}
