package cn.jdw.silky.client.config;

import feign.Logger;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * 声明 自动配置入口类
 * 创建时间 2021/6/26 13:29
 *
 * @author ListJiang
 */
@Configuration
@ComponentScan(basePackages = "cn.jdw.silky.client.*")
public class SilkyAutoConfiguration {

    @Bean
    @LoadBalanced
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    /**
     * @return Logger.Level配置
     */
    @Bean
    Logger.Level feignLoggerLevel() {
        return Logger.Level.FULL;
    }

}
