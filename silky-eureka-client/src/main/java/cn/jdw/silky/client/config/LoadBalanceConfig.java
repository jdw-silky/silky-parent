package cn.jdw.silky.client.config;

import io.github.resilience4j.circuitbreaker.CircuitBreakerConfig;
import io.github.resilience4j.timelimiter.TimeLimiterConfig;
import org.springframework.cloud.circuitbreaker.resilience4j.Resilience4JCircuitBreakerFactory;
import org.springframework.cloud.circuitbreaker.resilience4j.Resilience4JConfigBuilder;
import org.springframework.cloud.client.circuitbreaker.Customizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.time.Duration;
import java.util.concurrent.TimeoutException;

/**
 * 声明 负载均衡配置
 * 备注 官方给出的案例没有自定义负载均衡，后续看源码尝试实现
 * https://spring.io/guides/gs/spring-cloud-loadbalancer/
 * 创建时间 2021/6/26 13:26
 * @author ListJiang
 */
@Configuration
public class LoadBalanceConfig {
    @Bean
    public Customizer<Resilience4JCircuitBreakerFactory> defaultCustomizer() {
        return factory -> factory.configureDefault(id -> new Resilience4JConfigBuilder(id)
                .timeLimiterConfig(TimeLimiterConfig.custom().timeoutDuration(Duration.ofSeconds(2)).build())
                .circuitBreakerConfig(CircuitBreakerConfig.ofDefaults())
                .build());
    }

    /**
     * <img src ="https://files.readme.io/39cdd54-state_machine.jpg" alt="resilience4j"/>
     *
     * @return 时间长一点的断路器
     */
    @Bean
    public Customizer<Resilience4JCircuitBreakerFactory> slowCustomizer() {
        return factory -> factory.configure(builder -> builder.circuitBreakerConfig(CircuitBreakerConfig.ofDefaults())
                        .timeLimiterConfig(TimeLimiterConfig.custom().timeoutDuration(Duration.ofSeconds(4)).build()) // 超过4秒没有响应就认为请求失败
                        .circuitBreakerConfig(CircuitBreakerConfig.custom()
                                .failureRateThreshold(30.0f) // 失败率阈值设置，如果大于 30% 的失败了，就启动断路器
                                .slowCallRateThreshold(50.0f) // 慢调用阈值设置，如果大于 50% 的调用是慢调用，就启动断路器
                                .slowCallDurationThreshold(Duration.ofSeconds(2)) // 超过 2 秒的调用，就定为慢调用
                                .permittedNumberOfCallsInHalfOpenState(20) // 在半开状态允许调用，调用 20 次后重新判断阈值，决定断路器状态
                                .minimumNumberOfCalls(100) // 最小失败率计算呼叫次数，只有呼叫次数大于这个值才会计算失败率
                                .slidingWindowType(CircuitBreakerConfig.SlidingWindowType.TIME_BASED) // 滑动窗口类型
                                .slidingWindowSize(5) // 滑动窗口大小
                                .waitDurationInOpenState(Duration.ofSeconds(30)) // 断路器打开后保持 30 秒后切换为半开状态，又可以有限地发送请求
                                .recordExceptions(IOException.class, TimeoutException.class) // 设定计算失败数的异常，默认统计所有的异常
                                .ignoreExceptions(IllegalArgumentException.class) // 设定不计算失败数的异常，即不再 record 里面的也不在 ignore 里面的就不纳入故障率统计
                                .build())
                , "slow");
    }

}
